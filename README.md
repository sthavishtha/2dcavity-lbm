Lattice Boltzmann codes employing the open source OpenLB framework [(https://www.openlb.net/)](https://www.openlb.net/) v1.3-0 for simulating two-dimensional isothermal incompressible flows in lid-driven cavities. This includes cases of both uniform and oscillatory lid motions. Possibilities of using single-sided and two-sided wall motions also exist. In the two-sided case, the top and bottom lids are moving in the same (parallel wall motions) or opposite directions (anti-parallel wall motions). Sample postprocessing codes are provided, which have to be appropriately modified for your purpose. 

**Installation**

Refer the official OpenLB website [(https://www.openlb.net/tech-reports/)](https://www.openlb.net/tech-reports/) for more details about the installation of OpenLB package. 

![graphabs_antipar_par_comb](./images/graphabs_antipar_par_comb.png)

If you happen to use these codes in your work, please cite the reference below.

**Reference**

1. Sthavishtha R. Bhopalam, D. Arumuga Perumal, and Ajay Kumar Yadav, Computational appraisal of fluid flow behavior in two-sided oscillating lid-driven cavities, _Submitted for initial review to International Journal of Mechanical Sciences, Elsevier_, 2020.