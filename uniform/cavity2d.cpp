// A parallelized code for simulating flow in a 2D lid-driven cavity using the  
// interface of an open source package, OpenLB.
// Copyright (c) Sthavishtha Bhopalam Rajakumar
// Email : sthavishthabr@gmail.com
// Reference : extended from /examples/laminar/cavity2d/ code of OLB v1.3-1

#include "olb2D.h"
#ifndef OLB_PRECOMPILED // Unless precompiled version is used,
#include "olb2D.hh"     // include full template code
#endif
#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>

using namespace olb;
using namespace olb::descriptors;
using namespace olb::graphics;
using namespace olb::util;
using namespace std;

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif 

typedef double T;

#define MRT                                                                             // Comment/uncomment this line for BGK/MRT respectively
#ifdef MRT
#define DESCRIPTOR MRTD2Q9Descriptor 
#else
#define DESCRIPTOR D2Q9<>
#endif

#define VERIFY_GHIA                                                                  
//#define WriteCVelTime                                                                // uncomment for printing center-velocity at every time-step

T OTime[14] = {0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.7, 0.75, 0.8,                    // time-steps in an oscillating 
  0.85, 0.9, 0.95, 1.0};                                                                // lid-motion to be printed

void prepareGeometry(UnitConverter<T,DESCRIPTOR> const& converter,
                      SuperGeometry2D<T>& superGeometry, int LidType,
                      T ARatio) 
{
  OstreamManager clout(std::cout,"prepareGeometry");
  clout << "Prepare Geometry ..." << std::endl;

  superGeometry.rename(0,2);
  superGeometry.rename(2,1,1,1);
  superGeometry.clean();

  T eps = converter.getConversionFactorLength();                                          // = deltaX 

  Vector<T,2> extend(T(converter.getCharPhysLength()) + 2*eps, 2*eps);                    // extension length
  Vector<T,2> origin(T() - eps, 
    T(ARatio * converter.getCharPhysLength()) - eps);                                     // origin

  IndicatorCuboid2D<T> lidTop(extend, origin);                                            // center = origin + 0.5*(extension length)
  superGeometry.rename(2, 3, 1, lidTop);                                                  // Set material number for lid

  if (LidType == 2)                                                                       // 2 sided lid-motion
  {
    origin[0] = T() - eps;
    origin[1] = T() - eps;
    IndicatorCuboid2D<T> lidBot(extend, origin);                                          // center = origin + 0.5*(extension length)
    superGeometry.rename(2, 4, 1, lidBot);                                                // Set material number for lid
  }

  superGeometry.clean();                                                                  // Removes all not needed boundary voxels outside the surface
  superGeometry.innerClean();                                                             // Removes all not needed boundary voxels inside the surface
  superGeometry.checkForErrors();   
  superGeometry.getStatistics().print();
  clout << "Prepare Geometry ... OK" << std::endl;
}

void prepareLattice(UnitConverter<T,DESCRIPTOR> const& converter,
                    SuperLattice2D<T, DESCRIPTOR>& sLattice,
                    Dynamics<T, DESCRIPTOR>& bulkDynamics,
                    sOnLatticeBoundaryCondition2D<T,DESCRIPTOR>& sBoundaryCondition,
                    SuperGeometry2D<T>& superGeometry, int LidType) 
{

  OstreamManager clout(std::cout,"prepareLattice");
  clout << "Prepare Lattice ..." << std::endl;

  const T omega = converter.getLatticeRelaxationFrequency();

  // link lattice with dynamics for collision step

  // Material=0 -->do nothing
  sLattice.defineDynamics(superGeometry, 0, 
    &instances::getNoDynamics<T, DESCRIPTOR>());

  // Material=1 -->bulk dynamics
  sLattice.defineDynamics(superGeometry, 1, &bulkDynamics);

  // Material=2,3 -->bulk dynamics, velocity boundary
  sLattice.defineDynamics(superGeometry, 2, &bulkDynamics);
  sLattice.defineDynamics(superGeometry, 3, &bulkDynamics);

  if (LidType == 2)
    sLattice.defineDynamics(superGeometry, 4, &bulkDynamics);  

  sBoundaryCondition.addVelocityBoundary(superGeometry, 2, omega);
  sBoundaryCondition.addVelocityBoundary(superGeometry, 3, omega);

  if (LidType == 2)
    sBoundaryCondition.addVelocityBoundary(superGeometry, 4, omega);

  clout << "Prepare Lattice ... OK" << std::endl;
}

void setBoundaryValues(UnitConverter<T,DESCRIPTOR> const& converter,
                        SuperLattice2D<T, DESCRIPTOR>& sLattice,
                        int iT, SuperGeometry2D<T>& superGeometry,
                        int LidType, T velRatio, T omegaTop,
                        T omegaBot, char flowtypeTop, 
                        char flowtypeBot) 
{
 
  if (iT==0)                                                                                    // initial time step
  {    
    AnalyticalConst2D<T,T> rhoF(1);                                                               // set initial values: v = [0,0]
    std::vector<T> velocity(2,T());
    AnalyticalConst2D<T,T> uF(velocity);

    if (LidType == 1)
    {
      auto bulkIndicator = superGeometry.getMaterialIndicator({1, 2, 3});
      sLattice.iniEquilibrium(bulkIndicator, rhoF, uF);
      sLattice.defineRhoU(bulkIndicator, rhoF, uF);
    }

    else if (LidType == 2)
    {
      auto bulkIndicator = superGeometry.getMaterialIndicator({1, 2, 3, 4});
      sLattice.iniEquilibrium(bulkIndicator, rhoF, uF);
      sLattice.defineRhoU(bulkIndicator, rhoF, uF);      
    }

    velocity[0] = converter.getCharLatticeVelocity();                                                       // set lid velocity for upper boundary cells

    AnalyticalConst2D<T,T> uTop(velocity);
    sLattice.defineU(superGeometry, 3, uTop);                                                               //define u on material 3

    if (LidType == 2)
    {
      velocity[0] *= velRatio;
      AnalyticalConst2D<T,T> uBot(velocity);
      sLattice.defineU(superGeometry, 4, uBot);                                                              //define u on material 4  
    } 
    
    sLattice.initialize();                                                                                   // Make the lattice ready for simulation
  }

  else
  {
    if (flowtypeTop == 'O')
    {
      std::vector<T> velocityTop(2,T());        
      T thetaTop = omegaTop * (T) iT * converter.getPhysDeltaT();                                             // oscillating frequency -> angle
      velocityTop[0] = converter.getCharLatticeVelocity() * cos(thetaTop);
      AnalyticalConst2D<T,T> uTop(velocityTop);
      sLattice.defineU(superGeometry, 3, uTop);    
    } 

    if (flowtypeBot == 'O' && LidType == 2)
    {
      std::vector<T> velocityBot(2,T());
      T thetaBot = omegaBot * (T) iT * converter.getPhysDeltaT();                                             // converter.getPhysTime()
      velocityBot[0] = velRatio *                                                                         
        converter.getCharLatticeVelocity() * cos(thetaBot);
      AnalyticalConst2D<T,T> uBot(velocityBot);
      sLattice.defineU(superGeometry, 4, uBot);   
    } 
  }
} 

void getResults(SuperLattice2D<T, DESCRIPTOR>& sLattice,
                 UnitConverter<T,DESCRIPTOR> const& converter, int iT, 
                 Timer<T>* timer, const T logT, const T maxPhysT, 
                 const T imSave, const T vtkSave, std::string filenameGif,
                 std::string filenameVtk, std::string filenameTec,
                 std::string filenamecVel, std::string filenamexcVel,
                 std::string filenameycVel, const int timerPrintMode,
                 const int timerTimeSteps, const int resolx,              
                 const int resoly, SuperGeometry2D<T>& superGeometry,
                 bool converge, char flowtypeTop, char flowtypeBot,
                 T ARatio, int index = 0) 
{

  OstreamManager clout(std::cout,"getResults");
  SuperVTMwriter2D<T> vtmWriter(filenameVtk);

  if (iT==0) 
  {    
    SuperLatticeGeometry2D<T, DESCRIPTOR> geometry(sLattice, superGeometry);                            // Write the geometry, cuboid no. and rank no. 
    SuperLatticeCuboid2D<T, DESCRIPTOR> cuboid(sLattice);                                               // as vti file for visualization
    SuperLatticeRank2D<T, DESCRIPTOR> rank(sLattice);
    vtmWriter.write(geometry);
    vtmWriter.write(cuboid);
    vtmWriter.write(rank);
    vtmWriter.createMasterFile();
  }
  
  if (iT%converter.getLatticeTime(logT)==0 || converge) 
    sLattice.getStatistics().print(iT, converter.getPhysTime(iT));                                      // Prints statistics

  
  if (iT%timerTimeSteps==0 || (converge && flowtypeTop == 'O')) 
    timer->print(iT, timerPrintMode);                                                                   // Prints the passed/remaining time, MLUPS after timerTimesteps
  
  if ((iT%converter.getLatticeTime(vtkSave)==0 && iT>(int)(0.8 * maxPhysT)) 
    || converge)                                                                                        // Write the VTK files readable in ParaView
  {
    SuperLatticePhysVelocity2D<T,DESCRIPTOR> velocity(sLattice, converter);
    SuperLatticePhysPressure2D<T,DESCRIPTOR> pressure(sLattice, converter);
    vtmWriter.addFunctor(velocity);
    vtmWriter.addFunctor(pressure);
    vtmWriter.write(iT);
  }
  
  if (iT == converter.getLatticeTime(maxPhysT) || converge)                                             // Output velocity data in Tecplot readable file
  {
    SuperLatticePhysVelocity2D<T,DESCRIPTOR> velocity(sLattice, converter);
    
    BlockReduction2D2D<T> velocityPlane(velocity, resolx, BlockDataSyncMode::ReduceOnly);               // both x & y-resolution appropriately controlled          

    ofstream myfile;
    std::string newfilenameTec;                   

    if (flowtypeTop == 'O')
      newfilenameTec = singleton::directories().getGnuplotOutDir() + "data/" + 
        to_string((int)(OTime[index]*100.0)) + filenameTec;
    
    else if (flowtypeTop == 'S')
    {
      newfilenameTec = singleton::directories().getGnuplotOutDir() + "data/" 
        + filenameTec;        
    }

    if (singleton::mpi().isMainProcessor())
    {
      myfile.open (newfilenameTec.c_str(), ios::out);
      myfile << "TITLE=\"2D\" \n" << "VARIABLES=\"X\",\"Y\",\"U\",\"V\" \n" <<                                         // Tecplot header
        "ZONE T=\"BLOCK1\",I="<< velocityPlane.getBlockStructure().getNx() << 
        ",J=" << velocityPlane.getBlockStructure().getNy() << ",F=POINT" << endl;     

      int nXY[2] = {0};

      for (nXY[0]=0; nXY[0] < velocityPlane.getBlockStructure().getNx(); ++nXY[0])
        for (nXY[1]=0; nXY[1] < velocityPlane.getBlockStructure().getNy(); ++nXY[1])                      // Print the velocity data
        { 
          T vel[2] = {.0};
          velocityPlane(vel, nXY);

          myfile << std::setprecision(6)
            << nXY[0] << "\t" << nXY[1] << "\t"            
            << vel[0] << "\t" << vel[1] << endl;                      
        }
      myfile.close();      
    }
  }
  
  if (iT == converter.getLatticeTime(maxPhysT) || converge)                                             // Print center line velocity profiles
  {    
    SuperLatticePhysVelocity2D<T, DESCRIPTOR> velocity(sLattice, converter);                            // Gives access to velocity information on lattice          
    AnalyticalFfromSuperF2D<T> interpolation(velocity, true, 1);                                        // Interpolation functor with velocityField information
              
    std::string newfilenamexcVel, newfilenameycVel;

    if (flowtypeTop == 'O')
    {
      newfilenamexcVel = singleton::directories().getGnuplotOutDir() + "data/" + 
        to_string((int)(OTime[index]*100.0)) + filenamexcVel;
      newfilenameycVel = singleton::directories().getGnuplotOutDir() + "data/" + 
        to_string((int)(OTime[index]*100.0)) + filenameycVel;
    }
    
    else if (flowtypeTop == 'S')
    {
      newfilenamexcVel = singleton::directories().getGnuplotOutDir() + "data/" + 
        filenamexcVel;
      newfilenameycVel = singleton::directories().getGnuplotOutDir() + "data/" + 
        filenameycVel;        
    }

    ofstream myfileX, myfileY;
    myfileX.open (newfilenamexcVel.c_str(), ios::out);
    
    for (int nY = 0; nY < resoly; ++nY)                                                                       // centerline x-velocity profile
    {
      T position[2] = {0.5, ARatio * nY / ((T)resoly - 1.0)};
      for (int i = 0; i < 2; ++i) 
        position[i] = converter.getCharPhysLength() * position[i];
      T velocity[2] = {T(), T()}; 
      interpolation(velocity, position); 
      myfileX << std::setprecision(6) << position[1] << "\t" << 
        velocity[0] << endl;
    }

    myfileX.close();
    myfileY.open (newfilenameycVel.c_str(), ios::out);
    
    for (int nX = 0; nX < resolx; ++nX)                                                                       // centerline y-velocity profile
    {
      T position[2] = {nX / ((T)resolx - 1.0), ARatio * 0.5};
      for (int j = 0; j < 2; ++j) 
        position[j] = converter.getCharPhysLength() * position[j];        
      T velocity[2] = {T(), T()};
      interpolation( velocity, position ); 
      myfileY << std::setprecision(6) << position[0] << "\t" << 
        velocity[1] << endl;
    }      
    myfileY.close();
  }

#ifdef WriteCVelTime                                                                                          // Print center velocity data at all time instants                                
  SuperLatticePhysVelocity2D<T, DESCRIPTOR> velocityField(sLattice, converter);     
  AnalyticalFfromSuperF2D<T> interpolation(velocityField, true, 1);                                           // communicateToAll=true, overlap=1

  T position[2] = {0.5, 0.5 * ARatio * converter.getCharPhysLength()};                                        // center position of domain
  T velocity[2] = {T(), T()};

  interpolation(velocity, position);                                                                          // Interpolate velocityField at "position" and save it in "velocity"

  std::string newfilenamecVel;
  newfilenamecVel = singleton::directories().getGnuplotOutDir() + "data/" 
    + filenamecVel;     

  if (singleton::mpi().isMainProcessor())
  {
    ofstream myfile;
    myfile.open (newfilenamecVel.c_str(), ios::app);

    if (iT == 0)
    {
        myfile << "Iteration step" << "\t" << "Velocity_x" << 
          "\t" << "Velocity_y" << endl;
    }

    myfile << iT << "\t" << std::setprecision(6) 
      << velocity[0] << "\t" << velocity[1] << endl;
    myfile.close();    
  }
#endif

#ifdef VERIFY_GHIA
  if (iT == converter.getLatticeTime(maxPhysT) || converge) 
  {
    SuperLatticePhysVelocity2D<T, DESCRIPTOR> velocityField(sLattice, converter);
    AnalyticalFfromSuperF2D<T> interpolation(velocityField, true, 1);

    Vector<int,17> y_coord( {128, 125, 124, 123, 122, 109, 94, 79, 64, 58, 36,                                    // Data extracted from the paper :
      22, 13, 9, 8, 7, 0} );                                                                                      // "High-Re Solutions for Incompressible Flow Using the     
    Vector<T,17> vel_ghia_RE1000({  1.0,     0.65928, 0.57492, 0.51117, 0.46604,                                  // Navier-Stokes Equations and a Multigrid Method" by Ghia et al. 1982 
                                    0.33304, 0.18719, 0.05702,-0.06080,-0.10648,
                                    -0.27805,-0.38289,-0.29730,-0.22220,-0.20196,
                                    -0.18109, 0.0
                                  });
    Vector<T,17> vel_ghia_RE100({ 1.0,     0.84123, 0.78871, 0.73722, 0.68717,
                                  0.23151, 0.00332,-0.13641,-0.20581,-0.21090,
                                  -0.15662,-0.10150,-0.06434,-0.04775,-0.04192,
                                  -0.03717, 0.0
                                 });
    Vector<T,17> vel_simulation;
    
    static Gnuplot<T> gplot("centerVelocityX");                                                                   // Gnuplot interface to create plots
    
    Vector<T,17> comparison;                                                                                      // Define comparison values

    if ((int)converter.getReynoldsNumber() == 100) // Re100
      comparison = vel_ghia_RE100;
    else if ((int)converter.getReynoldsNumber() == 1000) // Re1000
      comparison = vel_ghia_RE1000;

    for (int nY = 0; nY < 17; ++nY) 
    {      
      T position[2] = {0.5, y_coord[nY]/128.0};                                                                   // 17 data points evenly distributed between 0 and 1 (height)
      T velocity[2] = {T(), T()};
      interpolation(velocity, position);      
      vel_simulation[nY] = velocity[0];                                                                           // Save x-velocity (in x-direction) in "vel_simulation" for every position "nY"      
      gplot.setData(position[1], {vel_simulation[nY],comparison[nY]}, {"simulated","Ghia"});                      // Set data for plot output - also writes the data into a datafile
    }  
    
    gplot.writePNG();                                                                                             // Create PNG filecenterVelocityX.png
    
    clout << "absoluteErrorL2(line)=" << (vel_simulation - comparison).norm() / 17. << 
      "; relativeErrorL2(line)=" << (vel_simulation - comparison).norm() / comparison.norm() 
      << std::endl;         
  }
#endif
}

int main(int argc, char* argv[]) 
{
  // === 1st Step: Initialization ===
  olbInit(&argc, &argv);
  OstreamManager clout(std::cout,"main");
  
  string fName("cavity2d.xml");                                                                   // XML filename to be read
  XMLreader config(fName);
  
  std::string olbdir, outputdir;                                                                  // Fix the olb and o/p directories            
  config["Application"]["OlbDir"].read(olbdir);
  config["Output"]["OutputDir"].read(outputdir);
  singleton::directories().setOlbDir(olbdir);
  singleton::directories().setOutputDir(outputdir);

  UnitConverter<T,DESCRIPTOR>* converter =                                                        // Read the unit converter related input from the XML file
    createUnitConverter<T,DESCRIPTOR>(config);  
  converter->print();                                                                              // Prints the converter log as console output
  converter->write("cavity2d");                                                                    // Writes the converter log in a file (.dat format)

  T ARatio = config["Application"]["AspectRatio"].get<T>();                                        // Aspect ratio of the cavity (deep/shallow)
  int N = converter->getResolution() + 1;                                                          // number of voxels in x direction
  int M = (int)ARatio*converter->getResolution() + 1;                                              // number of voxels in y direction
  Timer<T>* timer = createTimer<T>(config, *converter, N*M);

  int LidType = config["Application"]["FlowConfiguration"]["LidType"].get<int>();                   // Flow configuration
  
  char flowtypeTop = config["Application"]["FlowConfiguration"]["TopLid"]                           // Top lid
    ["FlowType"].get<char>();
  T OscillTimePeriod = config["Application"]["FlowConfiguration"]["TopLid"]
    ["OscillTimePeriod"].get<T>();
  T omegaTop = 2.0 * M_PI / OscillTimePeriod;
  
  char flowtypeBot = config["Application"]["FlowConfiguration"]["BottomLid"]                       // Bottom lid
    ["FlowType"].get<char>();
  OscillTimePeriod = config["Application"]["FlowConfiguration"]["BottomLid"]
    ["OscillTimePeriod"].get<T>();
  T omegaBot = 2.0 * M_PI / OscillTimePeriod;
  T velRatio = config["Application"]["FlowConfiguration"]["BottomLid"]
    ["VelocityRatio"].get<T>();

  T logT = config["Output"]["Log"]["SaveTime"].get<T>();                                            // o/p statistics after (s)
  T imSave = config["Output"]["VisualizationImages"]["SaveTime"].get<T>();                          // save the images after (s)  
  T vtkSave = config["Output"]["VisualizationVTK"]["SaveTime"].get<T>();                            // o/p data to vtk files after (s)
  T maxPhysT = config["Application"]["PhysParameters"]["PhysMaxTime"].get<T>();                     // entire simulation time(s)
  
  std::string filenameVtk = config["Output"]["VisualizationVTK"]                                    // Filenames for exporting output data
    ["Filename"].get<std::string>();                                                                // name of Vtk files
  std::string filenameGif = config["Output"]["VisualizationImages"]
    ["Filename"].get<std::string>();                                                                // name of Gif images
  std::string filenameTec = config["Output"]
    ["VisualizationTecplot"].get<std::string>();  
  std::string filenamexcVel = config["Output"]["VisualizationCenterVel"]
    ["xCenterlineVelocity"].get<std::string>();
  std::string filenameycVel = config["Output"]["VisualizationCenterVel"]
    ["yCenterlineVelocity"].get<std::string>();
  std::string filenamecVel = config["Output"]["VisualizationCenterVel"]
    ["CenterVelocity"].get<std::string>();
  std::string errorfilename = "error.txt";
                      
  int timerSkipType = config["Output"]["Timer"]["SkipType"].get<T>();
  int timerPrintMode = config["Output"]["Timer"]["PrintMode"].get<int>();                           // mode for printing time related statistics
  int timerTimeSteps = 1;                                                                           // timerTimeSteps = function of 1/deltaT
  if (timerSkipType == 0) 
  {
    timerTimeSteps = converter->getLatticeTime(config["Output"]["Timer"]
      ["PhysTime"].get<T>());
  }
  else
    config["Output"]["Timer"]["TimeSteps"].read(timerTimeSteps);

  // === 2rd Step: Prepare Geometry ===
  Vector<T,2> extend(1.0, ARatio);
  extend *= converter->getCharPhysLength();
  Vector<T,2> origin(0,0);
  IndicatorCuboid2D<T> cuboid(extend, origin); 

#ifdef PARALLEL_MODE_MPI
  CuboidGeometry2D<T> cuboidGeometry(cuboid, 
    converter->getConversionFactorLength(), singleton::mpi().getSize());
#else
  CuboidGeometry2D<T> cuboidGeometry(cuboid, 
    converter->getConversionFactorLength(), 7);
#endif

  cuboidGeometry.print();                                                                             // prints cuboid structure statistics

  HeuristicLoadBalancer<T> loadBalancer(cuboidGeometry);
  SuperGeometry2D<T> superGeometry(cuboidGeometry, loadBalancer, 2);
  prepareGeometry(*converter, superGeometry, LidType, ARatio);

  // === 3rd Step: Prepare Lattice ===
  SuperLattice2D<T, DESCRIPTOR> sLattice(superGeometry);
  Dynamics<T, DESCRIPTOR>* bulkDynamics;

#ifndef MRT
    bulkDynamics = new ConstRhoBGKdynamics<T, DESCRIPTOR> (
      converter->getLatticeRelaxationFrequency(), 
      instances::getBulkMomenta<T,DESCRIPTOR>());  
#else
    bulkDynamics = new MRTdynamics<T, DESCRIPTOR> (
      converter->getLatticeRelaxationFrequency(), 
      instances::getBulkMomenta<T,DESCRIPTOR>());      
#endif

  sOnLatticeBoundaryCondition2D<T,DESCRIPTOR> sBoundaryCondition(sLattice);

#ifndef MRT
    createInterpBoundaryCondition2D<T,DESCRIPTOR,
      ConstRhoBGKdynamics<T,DESCRIPTOR> > (sBoundaryCondition);
#else
    createInterpBoundaryCondition2D<T,DESCRIPTOR,MRTdynamics<T,DESCRIPTOR> > (sBoundaryCondition);
    // createLocalBoundaryCondition2D<T, DESCRIPTOR> (sBoundaryCondition);
#endif

  prepareLattice(*converter, sLattice, *bulkDynamics, sBoundaryCondition, 
    superGeometry, LidType);

  // === 4th Step: Main Loop with Timer ===
  int interval = converter->getLatticeTime(config["Application"]
    ["ConvergenceCheck"]["interval"].get<T>());                                               // check convergence after interval no. of time-steps
  T epsilon = config["Application"]["ConvergenceCheck"]["residuum"].get<T>();                 // convergence criterion

  bool converge = false;                                                                      // Track convergence
  int convergeLatTime = 0;
  T energy0 = sLattice.getStatistics().getAverageEnergy(); 
  T energy1 = 0.;
  int diffLatTime, incTime, index;

  timer->start();
  incTime = converter->getLatticeTime(OscillTimePeriod);                            // increment time = 1 osc. cycle time-period

  std::vector<int> LatOTime(14,int());   
  for (int k = 0; k < 14; ++k) 
    LatOTime[k] = converter->getLatticeTime(OTime[k]*OscillTimePeriod);

  for (int iT=0; iT <= converter->getLatticeTime(maxPhysT); ++iT) 
  {
    if (converge)                                                                           // attained convergence
    {
      if (iT == convergeLatTime + 1)
        clout << "Simulation converged." << endl;

      if (flowtypeTop == 'O')                                                               // oscillating lid motion
      {
        diffLatTime = iT - convergeLatTime  - 1;                                            // -1 to account for the next iteration step
        auto itr = std::find(LatOTime.begin(), LatOTime.end(), diffLatTime);                // find if diffLatTime exists in LatOTime

        if (itr != LatOTime.cend())                                                          
        {
          index = std::distance(LatOTime.begin(), itr);                                     // find the position of the LatOTime element
          clout << "time-step in the oscillating cycle = " << OTime[index] 
            << "T"<< endl;
          getResults(sLattice, *converter, iT, timer, logT, maxPhysT, imSave, 
                      vtkSave, filenameGif, filenameVtk, filenameTec,
                      filenamecVel, filenamexcVel, filenameycVel,
                      timerPrintMode, timerTimeSteps, N, M, superGeometry, 
                      converge, flowtypeTop, flowtypeBot, ARatio, index);
        }

        if (diffLatTime == incTime) break;                                                  // break the loop once the full cycle is over
      } 

      else if (flowtypeTop == 'S')                                                          // steady lid motion
      {
        getResults(sLattice, *converter, iT, timer, logT, maxPhysT, imSave,  
                    vtkSave, filenameGif, filenameVtk, filenameTec,  
                    filenamecVel, filenamexcVel, filenameycVel, timerPrintMode, 
                    timerTimeSteps, N, M, superGeometry, converge, flowtypeTop, 
                    flowtypeBot, ARatio);  
        break;
      }      
    }

    // === 5th Step: Definition of Initial and Boundary Conditions ===
    setBoundaryValues(*converter, sLattice, iT, superGeometry, LidType, velRatio, 
      omegaTop, omegaBot, flowtypeTop, flowtypeBot);
    
    // === 6th Step: Collide and Stream Execution ===
    sLattice.collideAndStream();
    
    // === 7th Step: Computation and Output of the Results ===
    if (!converge)
      getResults(sLattice, *converter, iT, timer, logT, maxPhysT, imSave, vtkSave, 
                  filenameGif, filenameVtk, filenameTec, filenamecVel, 
                  filenamexcVel, filenameycVel, timerPrintMode, timerTimeSteps,
                  N, M, superGeometry, converge, flowtypeTop, flowtypeBot, ARatio);
    
    if (iT%interval == 0)                                                                   //check for convergence after interval time-steps
    {
      energy1 = sLattice.getStatistics().getAverageEnergy();
      clout << "step=" << iT << "; Error=" << fabs((energy1 - energy0)/energy1)
        << std::endl;     

      if (singleton::mpi().isMainProcessor())
      {
        ofstream myfile;
        myfile.open (errorfilename.c_str(), ios::app);
        myfile << iT << "\t" << std::setprecision(6) 
          << fabs((energy1 - energy0)/energy1) << endl;
        myfile.close();          
      }       

      if (fabs((energy1 - energy0)/energy1) < epsilon && !converge) 
      {
        converge = true;
        convergeLatTime = iT;                                                               // converged time-step
      }

      energy0 = energy1;
    }   
  }

  timer->stop();
  timer->printSummary();
  delete converter;
  delete timer;
}
