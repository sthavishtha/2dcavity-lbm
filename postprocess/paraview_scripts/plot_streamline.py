### pvpython script to plot streamlines from velocity profile data (in Tecplot readable file)
### https://www.paraview.org/Wiki/ParaView_and_Batch
### run as pvpython (or pvbatch) name_of_this_file.py "$input_directory_name" "$tecplot_file_name" ...
### ... "output_directory_name" "output_plot_file_name"

# import the simple module from the paraview
from paraview.simple import *

# disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# user-entered input filenames, directories
directoryIn = sys.argv[1]
datasetIn = sys.argv[2]
directoryOut = sys.argv[3]
imageFileName = sys.argv[4]

# create a new 'Tecplot Reader'
velocityplt = TecplotReader(FileNames=[directoryIn + datasetIn + '.plt'])
velocityplt.DataArrayStatus = ['U', 'V']

# get active view
renderView1 = GetActiveViewOrCreate('RenderView')
# uncomment following to set a specific view size
renderView1.ViewSize = [1049, 485]
# renderView1.ViewSize = [500, 250]

# show data in view
velocitypltDisplay = Show(velocityplt, renderView1)
# trace defaults for the display properties.
velocitypltDisplay.Representation = 'Surface'
velocitypltDisplay.ColorArrayName = [None, '']
velocitypltDisplay.OSPRayScaleArray = 'U'
velocitypltDisplay.OSPRayScaleFunction = 'PiecewiseFunction'
velocitypltDisplay.SelectOrientationVectors = 'None'
velocitypltDisplay.ScaleFactor = 0.1
velocitypltDisplay.SelectScaleArray = 'None'
velocitypltDisplay.GlyphType = 'Arrow'
velocitypltDisplay.GlyphTableIndexArray = 'None'
velocitypltDisplay.DataAxesGrid = 'GridAxesRepresentation'
velocitypltDisplay.PolarAxes = 'PolarAxesRepresentation'
velocitypltDisplay.ScalarOpacityUnitDistance = 0.035076939009667914

# reset view to fit data
renderView1.ResetCamera()

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Calculator'
calculator1 = Calculator(Input=velocityplt)
calculator1.Function = ''

# Properties modified on calculator1
calculator1.ResultArrayName = 'vel'
calculator1.Function = 'U*iHat+V*jHat'

# show data in view
calculator1Display = Show(calculator1, renderView1)
# trace defaults for the display properties.
calculator1Display.Representation = 'Surface'
calculator1Display.ColorArrayName = [None, '']
calculator1Display.OSPRayScaleArray = 'U'
calculator1Display.OSPRayScaleFunction = 'PiecewiseFunction'
calculator1Display.SelectOrientationVectors = 'vel'
calculator1Display.ScaleFactor = 0.1
calculator1Display.SelectScaleArray = 'None'
calculator1Display.GlyphType = 'Arrow'
calculator1Display.GlyphTableIndexArray = 'None'
calculator1Display.DataAxesGrid = 'GridAxesRepresentation'
calculator1Display.PolarAxes = 'PolarAxesRepresentation'
calculator1Display.ScalarOpacityUnitDistance = 0.035076939009667914

# hide data in view
Hide(velocityplt, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Mask Points'
maskPoints1 = MaskPoints(Input=calculator1)

# Properties modified on renderView1.AxesGrid
renderView1.AxesGrid.Visibility = 1

# Properties modified on maskPoints1
maskPoints1.MaximumNumberofPoints = 100
maskPoints1.ProportionallyDistributeMaximumNumberOfPoints = 1
maskPoints1.RandomSampling = 1
maskPoints1.RandomSamplingMode = 'Spatially Stratified Random Sampling'

# show data in view
maskPoints1Display = Show(maskPoints1, renderView1)
# trace defaults for the display properties.
maskPoints1Display.Representation = 'Surface'
maskPoints1Display.ColorArrayName = [None, '']
maskPoints1Display.OSPRayScaleArray = 'U'
maskPoints1Display.OSPRayScaleFunction = 'PiecewiseFunction'
maskPoints1Display.SelectOrientationVectors = 'vel'
maskPoints1Display.ScaleFactor = 0.1
maskPoints1Display.SelectScaleArray = 'None'
maskPoints1Display.GlyphType = 'Arrow'
maskPoints1Display.GlyphTableIndexArray = 'None'
maskPoints1Display.DataAxesGrid = 'GridAxesRepresentation'
maskPoints1Display.PolarAxes = 'PolarAxesRepresentation'

# hide data in view
Hide(calculator1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# create a new 'Stream Tracer With Custom Source'
streamTracerWithCustomSource1 = StreamTracerWithCustomSource(Input=calculator1,
    SeedSource=maskPoints1)
streamTracerWithCustomSource1.Vectors = ['POINTS', 'vel']

# Properties modified on streamTracerWithCustomSource1
streamTracerWithCustomSource1.ComputeVorticity = 0

# show data in view
streamTracerWithCustomSource1Display = Show(streamTracerWithCustomSource1, renderView1)
# trace defaults for the display properties.
streamTracerWithCustomSource1Display.Representation = 'Surface'
streamTracerWithCustomSource1Display.ColorArrayName = [None, '']
streamTracerWithCustomSource1Display.OSPRayScaleArray = 'IntegrationTime'
streamTracerWithCustomSource1Display.OSPRayScaleFunction = 'PiecewiseFunction'
streamTracerWithCustomSource1Display.SelectOrientationVectors = 'vel'
streamTracerWithCustomSource1Display.ScaleFactor = 0.09981031403294766
streamTracerWithCustomSource1Display.SelectScaleArray = 'IntegrationTime'
streamTracerWithCustomSource1Display.GlyphType = 'Arrow'
streamTracerWithCustomSource1Display.GlyphTableIndexArray = 'IntegrationTime'
streamTracerWithCustomSource1Display.DataAxesGrid = 'GridAxesRepresentation'
streamTracerWithCustomSource1Display.PolarAxes = 'PolarAxesRepresentation'

# hide data in view
Hide(calculator1, renderView1)

# hide data in view
Hide(maskPoints1, renderView1)

# update the view to ensure updated data information
renderView1.Update()

# current camera placement for renderView1
renderView1.InteractionMode = '2D'
renderView1.CameraPosition = [0.5, 0.5, 2.7320508075688776]
renderView1.CameraFocalPoint = [0.5, 0.5, 0.0]
renderView1.CameraParallelScale = 0.7071067811865476

# save screenshot
SaveScreenshot(directoryOut + imageFileName + '.png', renderView1, ImageResolution=renderView1.ViewSize)