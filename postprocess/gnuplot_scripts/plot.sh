#!/bin/bash
# Run as bash plot.sh sr_01 
# If you want to print the bash output (including stderr), use :
# bash plot.sh sr_01 > >(tee output.log) 2>&1 
# https://stackoverflow.com/questions/18460186/writing-outputs-to-log-file-and-console

sr_case=$1
freq=("2pi_06" "2pi_1_2" "2pi_12" "2pi_6" "2pi_60")
re=("re100" "re400" "re1000")
#freq=("2pi_06" "2pi_1_2" "2pi_12" "2pi_6" "2pi_60" "2pi_06_v2" "2pi_6_v2")
#re=("re1000")

for j in "${re[@]}"
do 
	for i in "${freq[@]}"
	do
		echo "../../runs/par/$sr_case/${j}/${i}/"
#		gnuplot -c plot_error.gplt "../../runs/par/$sr_case/${j}/${i}/"
#		gnuplot -c plot_uv.gplt "../../runs/par/$sr_case/${j}/${i}/"
		gnuplot -c plot_cvel.gplt "../../runs/par/$sr_case/${j}/${i}/"
#		bash plot_streamline.sh "../../runs/par/$sr_case/${j}/${i}"
	done
done
