#!/bin/bash
# call as : bash plot_streamline.sh "../antipar/sr_1/re100/2pi_06"

filename=$1
dirname="$filename/tmp/imageData/data"

# replace the correct grid size in tepclot readable file
# https://stackoverflow.com/questions/15646800/how-can-i-replace-a-word-at-a-specific-line-in-a-file-in-unix
# https://stackoverflow.com/questions/525592/find-and-replace-inside-a-text-file-from-a-bash-command
for i in {20..50..5} {70..100..5}
do 
#	echo "sed -i -e '/^ZONE/ s/258/257/' $dirname/${i}velocity.plt"
	sed -i -e '/^ZONE/ s/258/257/' $dirname/${i}velocity.plt
done
echo "Replaced correct grid sizes in tecplot readable files"

# plot streamlines in paraview 
for i in {20..50..5} {70..100..5}
do 
# 	echo "pvpython plot_streamline.py "./$dirname/" "${i}velocity" "./$dirname/" "${i}velocity""
	pvpython plot_streamline.py "./$dirname/" "${i}velocity" "./$dirname/" "${i}velocity"
	clear
done
echo "Streamlines plotted in paraview"
